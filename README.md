# Accyourate API Documentation #

WARNING: This documentation (and API) are a "work in progress".

### Useful links ###
* Postman: Great tool for testing API ( [Download Here](https://www.postman.com/) )

### Who do I talk to? ###

* Project Manager & CTO: [Gian Angelo Geminiani](mailto:angelo.geminiani@accyourate.com)
* Developer & Data Scientist: [Antonio Augello](mailto:antonio.augello@accyourate.com)

### API Access Token ###

Before use API you need ask an Access Token. 

### Changelog ###
* v1.0: [View Documentation](./api_v1.md)

